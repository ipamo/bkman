#!/bin/sh
if [ $(id -u) -ne 0 ]; then
    printf "$RED%s$NC\n" "This script must be run as root." >&2
    exit 1
fi

DIR=${DIR:-/root/.local/info}
mkdir -p "$DIR"

# General info
echo "hostname: $(hostnamectl)" > "$DIR/general.txt"
echo "uname: $(uname -a)" >> "$DIR/general.txt"
echo "uptime: $(uptime)" >> "$DIR/general.txt"

df -h > "$DIR/df.txt"
free -h > "$DIR/free.txt"

# Packages
dpkg-query --list > "$DIR/dpkg-query.txt"
apt list --installed 2> /dev/null > "$DIR/apt-list.txt"

# Systemd
systemctl --all > "$DIR/systemd-all.txt"
systemctl status > "$DIR/systemd-status.txt"
systemd-analyze plot > "$DIR/systemd-plot.svg"

# Networking
ip addr > "$DIR/ip-addr.txt"
ip route > "$DIR/ip-route.txt"
lsof -i -P -n > "$DIR/net-lsof.txt"
netstat -pln > "$DIR/netstat.txt"
which fw > /dev/null && fw status > "$DIR/fw-status.txt"

# Alternatives
ls -la /etc/alternatives > "$DIR/alternatives.txt"

# Docker
if which docker > /dev/null; then
    docker ps -a > "$DIR/docker-ps.txt"
    docker images -a > "$DIR/docker-images.txt"
    docker network ls > "$DIR/docker-networks.txt"
    docker volume ls > "$DIR/docker-volumes.txt"
fi
