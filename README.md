Bkman
=====

Easy backup and restore using Borg.

Documentation:

- `./bk -h` for main script help.
- [Borg usage](articles/borg.md) for creation and management of repository.

Deploy on local system:

- Add to PATH: `ln -s /opt/bkman/bk /usr/local/bin/bk` if repository is in /opt/bkman/bk
- Create /root/.config/borg/keys directory and add your borg repository key
- Create /root/.config/borg/default.env (see: examples/borg/default.env)
- Create /root/.ssh (see: examples/ssh)
- Create /bk.list (see: examples/bk.list)
- Create /etc/cron.d/bk-crontab (see: examples/bk-crontab)

Build Docker image:

- Create docker/borg/keys directory and add your borg repository key
- Create docker/borg/default.env (see: examples/borg/default.env)
- Create docker/.ssh (see: examples/ssh)
- Run `docker build -t bkman .`

Run `bk` (or directly `borg`) through Docker:

    docker run --rm bkman bk -h
    docker run --rm bkman borg -h
