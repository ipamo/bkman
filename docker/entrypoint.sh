#!/bin/sh
exit_with_message() {
    local code=$1
    local nb_dir=$2
    if [ $nb_dir -eq 0 ]; then
        echo "no volume treated: /mnt is empty" >&2
        exit 3
    fi

    if [ $code -eq 0 ]; then
        echo "$nb_dir volume(s): success" >&2
    elif [ $code -eq 1 ]; then
        echo "$nb_dir volume(s): warnings" >&2
    else
        echo "$nb_dir volume(s): errors" >&2
    fi
}

backup_mnt() {
    local nb_dir=0
    local global_code=0
    local subdir
    for subdir in $(ls -1 /mnt); do
        nb_dir=$(($nb_dir+1))

        local prefix="$subdir"
        local dir="/mnt/$subdir"

        bk backup -p "$prefix" "$dir" "$@"

        local code=$?
        if [ $code -gt $global_code ]; then
            global_code=$code
        fi
    done

    exit_with_message $global_code $nb_dir
}

restore_mnt() {
    local nb_dir=0
    local global_code=0
    local subdir
    for subdir in $(ls -1 /mnt); do
        nb_dir=$(($nb_dir+1))

        local prefix="$subdir"
        local dir="/mnt/$subdir"

        bk restore -p "$prefix" "$dir" "$@"

        local code=$?
        if [ $code -gt $global_code ]; then
            global_code=$code
        fi
    done

    exit_with_message $global_code $nb_dir
}

if [ "$1" = "backup" ]; then
    shift
    backup_mnt "$@"
elif [ "$1" = "restore" ]; then
    shift
    restore_mnt "$@"
elif [ "$1" = "borg" ]; then
    . /root/.config/borg/default.env
    exec "$@"
elif [ -n "$1" ]; then
    exec "$@"
else
    exec /bin/sh
fi
