FROM alpine:3.11.6

# Configure shell
ENV ENV=/etc/profile
RUN echo "alias l='ls -l'" >>/etc/profile.d/aliases.sh

RUN apk add --no-cache borgbackup openssh-client \
    # for pre-backup scripts
    postgresql-client mariadb-client sqlite \
    # for chronic util
    moreutils

COPY bk /usr/local/bin/
COPY docker/.ssh /root/.ssh
COPY docker/borg /root/.config/borg
COPY docker/entrypoint.sh /entrypoint.sh

RUN chmod 600 /root/.ssh/* &&\
    chmod 600 /root/.config/borg/keys/* &&\
    chmod +x /usr/local/bin/bk &&\
    chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
