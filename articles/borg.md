Borg usage
==========

## Configuration du dépôt Borg

Créer le dépôt Borg

    sudo borg init -e keyfile --make-parent-dirs /srv/borgrepo

Make a backup copy of the key file and/or save it in your Keepass database / Wallet
(key file is located in: `~/.config/borg/keys`):

    sudo borg key export /srv/borgrepo .../borgrepo-my-host.key

WARNING: keys are per-repository.

Note: il est possible de changer la passphrase de la clé locale :

    sudo borg key change-passphrase /srv/borgrepo

Créer un utilisateur pour l'accès SSH :

    sudo useradd -rMU -s /bin/bash borgrepo
    sudo chown -R borgrepo:borgrepo /srv/borgrepo
    vim /etc/passwd # set home repository to /srv/borgrepo

Créer le fichier `/etc/ssh/authorized_keys_borgrepo` avec la clé publique pour
la connexion à l'utilisateur borgrepo.

Ajouter à `/etc/ssh/sshd_config`:

```
Match User borgrepo
    AuthorizedKeysFile  /etc/ssh/authorized_keys_%u
```

## Utilisation de base

Sauvegarder une archive :

    sudo borg create /srv/borgrepo::{hostname}_{now} \
        /home \
        /root \
        /etc \
        -e sh:/home/*/.cache \
        -e sh:/home/*/.thumbnails
    
Restaurer une archive :

    mkdir -p ~/borg-extract
    cd ~/borg-extract && sudo borg extract /srv/borgrepo::archive_name

Monter une archive :

    mkdir -p ~/borg-mount
    
    sudo borg mount /srv/borgrepo::archive_name ~/borg-mount

    ls ~/borg-mount

    sudo borg umount ~/borg-mount

Afficher les infos :

    sudo borg list /srv/borgrepo
    sudo borg info /srv/borgrepo::archive_name
